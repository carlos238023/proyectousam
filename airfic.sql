-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-03-2019 a las 22:47:07
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `airfic`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`kz`@`%` PROCEDURE `boleto` (IN `xid_usuario` INT, IN `xN_1` VARCHAR(45), IN `xN_2` VARCHAR(45), IN `xA_1` VARCHAR(45), IN `xA_2` VARCHAR(45), IN `xTelefono` VARCHAR(20), IN `xDireccion` VARCHAR(125), IN `xT_documento` INT(11), IN `xDocumento` VARCHAR(45), IN `xvalor` DOUBLE, IN `xfecha_vuelo` VARCHAR(45), IN `Xiva` DOUBLE, IN `xTotal` DOUBLE, IN `xHora_salida` VARCHAR(20), IN `xdestino` INT(11), IN `xcodigo` INT(11), IN `xTipo_pago` INT(11))  BEGIN

insert into pasajero (id_usuario ,nombre_uno,nombre_dos,apellido_uno ,apellido_dos,telefono,direccion ,tipo_documento ,numero_documento) values (xid_usuario,xN_1,xN_2,xA_1,xA_2,xTelefono,xDireccion,xT_documento,xDocumento);

insert into tiquete (id_pasajero,valor, fecha_vuelo,iva, total,hora_salida, desde_hacia,codigo, tipo_pago) values (last_insert_id(),xvalor,xfecha_vuelo,Xiva,xTotal,xHora_salida,xdestino,xcodigo,xTipo_pago);

END$$

CREATE DEFINER=`kz`@`%` PROCEDURE `prueba` (IN `xPago` VARCHAR(100))  BEGIN
insert into tipo_pago (pago) values (xPago); 
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aerolinea`
--

CREATE TABLE `aerolinea` (
  `id_aerolinea` int(11) NOT NULL,
  `nombre_aerolinea` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aerolinea`
--

INSERT INTO `aerolinea` (`id_aerolinea`, `nombre_aerolinea`) VALUES
(11, 'Avianca-TACA'),
(12, 'ES Airlines'),
(18, 'United Airlines'),
(19, 'CA Airline'),
(20, 'ACahuate Airlines'),
(22, 'United Airlinesss'),
(23, 'ES Airlines');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `avion`
--

CREATE TABLE `avion` (
  `id_avion` int(11) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `placa` varchar(45) NOT NULL,
  `aerolinea` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `avion`
--

INSERT INTO `avion` (`id_avion`, `capacidad`, `placa`, `aerolinea`) VALUES
(1, 475, 'YS-00001a', 11),
(2, 550, 'YS-00002a', 11),
(3, 475, 'YS-00003a', 11),
(5, 550, 'YS-00002a', 11),
(6, 500, 'AS400', 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `avion_destino`
--

CREATE TABLE `avion_destino` (
  `id_avion_destino` int(11) NOT NULL,
  `id_avion` int(11) NOT NULL,
  `id_origen_destino` int(11) NOT NULL,
  `tipo_vuelo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `avion_destino`
--

INSERT INTO `avion_destino` (`id_avion_destino`, `id_avion`, `id_origen_destino`, `tipo_vuelo`) VALUES
(3, 1, 2, 3),
(4, 2, 3, 3),
(5, 3, 4, 3),
(6, 2, 14, 4),
(8, 1, 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clase`
--

CREATE TABLE `clase` (
  `id_clase` int(11) NOT NULL,
  `clase` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clase`
--

INSERT INTO `clase` (`id_clase`, `clase`) VALUES
(6, 'economica'),
(7, 'ejecutiva'),
(8, 'primera clase');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destino`
--

CREATE TABLE `destino` (
  `id_destino` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `photo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `destino`
--

INSERT INTO `destino` (`id_destino`, `nombre`, `photo`) VALUES
(1, 'Panama, cuidad de panama', 'cuidadpanama.jpg'),
(2, 'Costa rica, san jose juan santamaria', 'costarica.jpg'),
(3, 'Mexico, cuidad de mexico', 'cuidadmexico.jpg'),
(4, 'Guatemala, ciudad de guatemala', 'cuidadmexico.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_usuario`
--

CREATE TABLE `login_usuario` (
  `id_login` int(11) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `pass` varchar(45) NOT NULL,
  `tipo_usuario` int(11) NOT NULL,
  `pregunta` varchar(45) DEFAULT NULL,
  `respuesta` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `login_usuario`
--

INSERT INTO `login_usuario` (`id_login`, `usuario`, `pass`, `tipo_usuario`, `pregunta`, `respuesta`) VALUES
(3, 'jony', '123', 4, '¿Cual es su año de nacimiento?', '1999'),
(4, 'roberto', '123', 4, '¿Cual es su año de nacimiento?', '1999'),
(5, 'sergio', '123', 4, '¿Cual fue su primera mascota?', 'zancudo'),
(7, 'geo', '123', 4, NULL, NULL),
(8, 'omar', '123', 4, '¿Cual fue programa de television favorito?', 'candy'),
(9, 'josue', '123', 4, '¿Cual es su año de nacimiento?', '1999'),
(12, 'geo', '123', 3, NULL, NULL),
(15, 'asaber', '123', 3, NULL, NULL),
(16, 'geovanny', '123', 3, NULL, NULL),
(17, 'geo', '123', 3, NULL, NULL),
(18, 'geo', '1234', 3, NULL, NULL),
(19, 'asaber1', '123', 4, NULL, NULL),
(20, 'jonyG', '123', 3, NULL, NULL),
(21, 'geo', '12345', 3, NULL, NULL),
(22, 'jonathan', '123', 4, '¿Cual fue su primera mascota?', 'perro'),
(23, 'jony2', '123', 4, NULL, NULL),
(24, 'geo', '12345', 3, NULL, NULL),
(25, 'Omarr', '1234', 3, NULL, NULL),
(26, 'omar', '123', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `ofertasvuelos`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `ofertasvuelos` (
`id_avion_destino` int(11)
,`id_origen_destino` int(11)
,`id_origen` int(11)
,`id_destino` int(11)
,`placa` varchar(45)
,`nombre_aerolinea` varchar(45)
,`vuelo` varchar(45)
,`clase` varchar(45)
,`origen` varchar(45)
,`destino` varchar(45)
,`precio` double
,`photo` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `origen`
--

CREATE TABLE `origen` (
  `id_origen` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `origen`
--

INSERT INTO `origen` (`id_origen`, `nombre`) VALUES
(1, 'el salvador, san salvador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `origen_destino`
--

CREATE TABLE `origen_destino` (
  `id_origen_destino` int(11) NOT NULL,
  `id_origen` int(11) DEFAULT NULL,
  `id_destino` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `clase` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `origen_destino`
--

INSERT INTO `origen_destino` (`id_origen_destino`, `id_origen`, `id_destino`, `precio`, `clase`) VALUES
(2, 1, 1, 298.45, 6),
(3, 1, 2, 293.73, 6),
(4, 1, 3, 291.17, 6),
(5, 1, 4, 331.71, 6),
(6, 1, 1, 310.45, 7),
(7, 1, 2, 299.73, 7),
(8, 1, 3, 250.17, 7),
(9, 1, 4, 315.71, 7),
(10, 1, 1, 370.71, 8),
(11, 1, 2, 327.73, 8),
(12, 1, 3, 291.17, 8),
(13, 1, 4, 352.71, 8),
(14, 1, 1, 500, 7),
(16, 1, 1, 250.5, 6),
(17, 1, 1, 290.5, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasajero`
--

CREATE TABLE `pasajero` (
  `id_pasajero` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `nombre_uno` varchar(45) DEFAULT NULL,
  `nombre_dos` varchar(45) DEFAULT NULL,
  `apellido_uno` varchar(45) DEFAULT NULL,
  `apellido_dos` varchar(45) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `direccion` varchar(125) DEFAULT NULL,
  `tipo_documento` int(11) DEFAULT NULL,
  `numero_documento` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pasajero`
--

INSERT INTO `pasajero` (`id_pasajero`, `id_usuario`, `nombre_uno`, `nombre_dos`, `apellido_uno`, `apellido_dos`, `telefono`, `direccion`, `tipo_documento`, `numero_documento`) VALUES
(23, 3, 'Bryan', 'David', 'Rodriguez', 'Carrillo', '7821-1232', 'soyapango', 3, '0001232334-1'),
(114, 3, 'jonathan', 'josue', 'rodriguez', 'carrillo', '7818-1319', 'soyapango', 3, '000000000001'),
(115, 3, 'jonathan', 'josue', 'rodriguez', 'carrillo', '7818-1319', 'soyapango', 3, '000000000001'),
(116, 3, 'jonathan', 'josue', 'rodriguez', 'carrillo', '7818-1319', 'soyapango', 3, '000000000001'),
(117, 3, 'jonathan', 'josue', 'rodriguez', 'carrillo', '7818-1319', 'soyapango', 3, '000000000001'),
(118, 4, 'ERFERF', 'FDRGDFGFD', 'FDGFDGDFGDF', 'DFG', '8654-5453', 'SEFSDRF', 3, 'FSDFSDF'),
(119, 3, 'asdasd', 'asdasd', 'dasd', 'asdasd', '1231-2312', 'asdasd', 3, '123123'),
(120, 3, '123123', '123', 'asdasd', 'asdasd', '1231-2312', 'asdasdasd', 3, '123123'),
(121, 3, 'asdas', 'asda', 'asdasd', 'asdasd', '1231-2312', 'asdasd', 3, '123123'),
(122, 3, 'asdas', 'asda', 'asdasd', 'asdasd', '1231-2312', 'asdasd', 3, '123123'),
(123, 3, 'asd', 'asd', 'asd', 'asd', '1231-2312', 'asdasd', 3, '123123'),
(124, 3, 'asdasdas', 'asdasd', 'asda', 'asd', '1231-2312', 'asdasd', 3, '123123'),
(125, 3, 'asda', 'asd', 'asda', 'sada', '1231-2312', 'asdasd', 3, '123123'),
(126, 3, 'asdasd', 'asdasd', 'asdasd', 'asd', '1231-2312', 'asadsd', 3, 'asdasd'),
(127, 3, 'asdasd', 'asda', 'sd', 'asd', '1231-2312', 'sadasd', 3, '12312'),
(128, 3, 'asd', 'asd', 'asd', 'asd', '1231-2312', 'asdasd', 3, '123123'),
(129, 4, 'gfdg', 'fdgsdfg', 'sdfgsdfg', 'dfgsdfg', '2222-2222', 'dsafdasdf', 3, '2222222222'),
(130, 3, 'asdasd', 'asd', 'asd', 'asd1', '2131-2312', 'asdasdas', 3, '123123'),
(131, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', 'asdas', 3, '123123'),
(132, 3, '123123', 'asdasd', 'asdasd', 'asdasd', '1231-2312', 'asdasd', 3, '123123'),
(133, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', 'asdasdasd', 3, '123123'),
(134, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', 'asdasdasd', 3, '123123'),
(135, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', 'asdasdasd', 3, '123123'),
(136, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', 'asdasdasd', 3, '123123'),
(137, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', 'asdasdasd', 3, '123123'),
(138, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', 'asdasdasd', 3, '123123'),
(139, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', 'asdasdasd', 3, '123123'),
(140, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', '123123', 3, '3123123'),
(141, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', '123123', 3, '3123123'),
(142, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', '123123', 3, '3123123'),
(143, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', '123123', 3, '3123123'),
(144, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', '123123', 3, '3123123'),
(145, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', '123123', 3, '3123123'),
(146, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', '123123', 3, '3123123'),
(147, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', '123123', 3, '3123123'),
(148, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', '123123', 3, '3123123'),
(149, 3, 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1231-2312', 'asdasd', 3, '123123'),
(150, 3, 'sasd', 'asdasdasd', 'asdasd', 'asdasdasd', '1231-2312', 'asdasdasd', 3, '123123123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `id_documento` int(11) NOT NULL,
  `nombre_documento` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`id_documento`, `nombre_documento`) VALUES
(3, 'pasaporte'),
(4, 'visa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `id_tipo_pago` int(11) NOT NULL,
  `pago` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_pago`
--

INSERT INTO `tipo_pago` (`id_tipo_pago`, `pago`) VALUES
(1, 'tarjeta de credito'),
(5, 'efectivo'),
(6, 'algo'),
(9, 'alma');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id_tipo` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id_tipo`, `tipo`) VALUES
(3, 'administrador'),
(4, 'usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_vuelo`
--

CREATE TABLE `tipo_vuelo` (
  `id_tipo_vuelo` int(11) NOT NULL,
  `vuelo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_vuelo`
--

INSERT INTO `tipo_vuelo` (`id_tipo_vuelo`, `vuelo`) VALUES
(3, 'directo'),
(4, 'ida y vuelta'),
(5, 'low cost'),
(6, 'internacionales'),
(7, 'escala');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiquete`
--

CREATE TABLE `tiquete` (
  `id_tiquete` int(11) NOT NULL,
  `id_pasajero` int(11) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `fecha_vuelo` varchar(45) DEFAULT NULL,
  `numero_puesto` int(11) DEFAULT NULL,
  `iva` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `hora_salida` varchar(20) DEFAULT NULL,
  `desde_hacia` int(11) DEFAULT NULL,
  `codigo` int(11) DEFAULT NULL,
  `tipo_pago` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tiquete`
--

INSERT INTO `tiquete` (`id_tiquete`, `id_pasajero`, `valor`, `fecha_vuelo`, `numero_puesto`, `iva`, `total`, `hora_salida`, `desde_hacia`, `codigo`, `tipo_pago`) VALUES
(16, 23, 123, '123', 1, 123, 123, '123', 3, 123234, 1),
(88, 114, 298.45, '2312/31/23', 2, 38.7985, 337.2485, '6:00 AM', 3, 1671517, 1),
(89, 115, 298.45, '1231/23/12', 3, 38.7985, 337.2485, '6:00 AM', 3, 167779, 1),
(90, 116, 298.45, '1231/23/12', 4, 38.7985, 337.2485, '6:00 AM', 3, 167814, 1),
(91, 117, 298.45, '1231/23/12', 5, 38.7985, 337.2485, '6:00 AM', 3, 167308, 1),
(92, 118, 293.73, '5324/53/21', 6, 38.184900000000006, 331.91490000000005, '6:00 AM', 4, 911863, 1),
(93, 119, NULL, '4574/56/45', 7, 38.7985, 337.2485, '6:00 AM', 3, 127233, 1),
(94, 120, 298.45, '1231/23/12', 8, 38.7985, 337.2485, '6:00 AM', 3, 1272018, 1),
(95, 121, 298.45, '1231/23/12', 9, 38.7985, 337.2485, '6:00 AM', 3, 1171988, 1),
(96, 122, 293.73, '2342/34/23', 10, 38.184900000000006, 331.91490000000005, '6:00 AM', 4, 1171287, 1),
(97, 123, 293.73, '2131/23/12', 11, 38.184900000000006, 331.91490000000005, '6:00 AM', 4, 671098, 1),
(98, 124, 291.17, '1231/23/12', 12, 37.8521, 329.0221, '6:00 AM', 5, 117462, 1),
(99, 125, 298.45, '1231/23/12', 13, 38.7985, 337.2485, '6:00 AM', 3, 8720952, 1),
(100, 126, 298.45, '1231/23/12', 14, 38.7985, 337.2485, '6:00 AM', 3, 9719996, 1),
(101, 127, 298.45, '1231/23/12', 15, 38.7985, 337.2485, '6:00 AM', 3, 9720811, 1),
(102, 128, 298.45, '1231/23/12', 16, 38.7985, 337.2485, '6:00 AM', 3, 6720592, 1),
(103, 129, 298.45, '2222/22/22', 17, 38.7985, 337.2485, '6:00 AM', 3, 0, 1),
(104, 130, 298.45, '1231/23/12', 18, 38.7985, 337.2485, '6:00 AM', 3, 1071456, 1),
(105, 131, 298.45, '1231/23/12', 19, 38.7985, 337.2485, '6:00 AM', 3, 1271484, 1),
(106, 132, 298.45, '1231/23/12', 20, 38.7985, 337.2485, '6:00 AM', 3, 1271141, 1),
(107, 149, 298.45, '1231/23/12', 21, 38.7985, 337.2485, '6:00 AM', 3, 1271462, 1),
(108, 150, 293.73, '1231/23/12', 22, 38.184900000000006, 331.91490000000005, '6:00 AM', 4, 1371957, 1);

--
-- Disparadores `tiquete`
--
DELIMITER $$
CREATE TRIGGER `tiquete_BEFORE_INSERT` BEFORE INSERT ON `tiquete` FOR EACH ROW BEGIN
set new.numero_puesto = (select max(numero_puesto) from tiquete) + 1;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vuelosv`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vuelosv` (
`id_avion_destino` int(11)
,`id_origen_destino` int(11)
,`id_origen` int(11)
,`id_destino` int(11)
,`placa` varchar(45)
,`nombre_aerolinea` varchar(45)
,`vuelo` varchar(45)
,`clase` varchar(45)
,`origen` varchar(45)
,`destino` varchar(45)
,`precio` double
,`photo` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `ofertasvuelos`
--
DROP TABLE IF EXISTS `ofertasvuelos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kz`@`%` SQL SECURITY DEFINER VIEW `ofertasvuelos`  AS  select `av`.`id_avion_destino` AS `id_avion_destino`,`o`.`id_origen_destino` AS `id_origen_destino`,`origen`.`id_origen` AS `id_origen`,`destino`.`id_destino` AS `id_destino`,`avion`.`placa` AS `placa`,`ar`.`nombre_aerolinea` AS `nombre_aerolinea`,`tipo`.`vuelo` AS `vuelo`,`clase`.`clase` AS `clase`,`origen`.`nombre` AS `origen`,`destino`.`nombre` AS `destino`,`o`.`precio` AS `precio`,`destino`.`photo` AS `photo` from (((((((`avion_destino` `av` join `avion` on((`av`.`id_avion` = `avion`.`id_avion`))) join `origen_destino` `o` on((`av`.`id_origen_destino` = `o`.`id_origen_destino`))) join `origen` on((`o`.`id_origen` = `origen`.`id_origen`))) join `destino` on((`o`.`id_destino` = `destino`.`id_destino`))) join `tipo_vuelo` `tipo` on((`av`.`tipo_vuelo` = `tipo`.`id_tipo_vuelo`))) join `aerolinea` `ar` on((`avion`.`aerolinea` = `ar`.`id_aerolinea`))) join `clase` on((`o`.`clase` = `clase`.`id_clase`))) where (`o`.`precio` < 300) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vuelosv`
--
DROP TABLE IF EXISTS `vuelosv`;

CREATE ALGORITHM=UNDEFINED DEFINER=`kz`@`%` SQL SECURITY DEFINER VIEW `vuelosv`  AS  select `av`.`id_avion_destino` AS `id_avion_destino`,`o`.`id_origen_destino` AS `id_origen_destino`,`origen`.`id_origen` AS `id_origen`,`destino`.`id_destino` AS `id_destino`,`avion`.`placa` AS `placa`,`ar`.`nombre_aerolinea` AS `nombre_aerolinea`,`tipo`.`vuelo` AS `vuelo`,`clase`.`clase` AS `clase`,`origen`.`nombre` AS `origen`,`destino`.`nombre` AS `destino`,`o`.`precio` AS `precio`,`destino`.`photo` AS `photo` from (((((((`avion_destino` `av` join `avion` on((`av`.`id_avion` = `avion`.`id_avion`))) join `origen_destino` `o` on((`av`.`id_origen_destino` = `o`.`id_origen_destino`))) join `origen` on((`o`.`id_origen` = `origen`.`id_origen`))) join `destino` on((`o`.`id_destino` = `destino`.`id_destino`))) join `tipo_vuelo` `tipo` on((`av`.`tipo_vuelo` = `tipo`.`id_tipo_vuelo`))) join `aerolinea` `ar` on((`avion`.`aerolinea` = `ar`.`id_aerolinea`))) join `clase` on((`o`.`clase` = `clase`.`id_clase`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aerolinea`
--
ALTER TABLE `aerolinea`
  ADD PRIMARY KEY (`id_aerolinea`);

--
-- Indices de la tabla `avion`
--
ALTER TABLE `avion`
  ADD PRIMARY KEY (`id_avion`),
  ADD KEY `aerolinea_idx` (`aerolinea`);

--
-- Indices de la tabla `avion_destino`
--
ALTER TABLE `avion_destino`
  ADD PRIMARY KEY (`id_avion_destino`),
  ADD KEY `id_avion_idx` (`id_avion`),
  ADD KEY `fk_avion_destino_tipo_vuelo1_idx` (`tipo_vuelo`),
  ADD KEY `fk_avion_destino_origen_destino1_idx` (`id_origen_destino`);

--
-- Indices de la tabla `clase`
--
ALTER TABLE `clase`
  ADD PRIMARY KEY (`id_clase`);

--
-- Indices de la tabla `destino`
--
ALTER TABLE `destino`
  ADD PRIMARY KEY (`id_destino`);

--
-- Indices de la tabla `login_usuario`
--
ALTER TABLE `login_usuario`
  ADD PRIMARY KEY (`id_login`),
  ADD KEY `tipo_usuario_idx` (`tipo_usuario`);

--
-- Indices de la tabla `origen`
--
ALTER TABLE `origen`
  ADD PRIMARY KEY (`id_origen`);

--
-- Indices de la tabla `origen_destino`
--
ALTER TABLE `origen_destino`
  ADD PRIMARY KEY (`id_origen_destino`),
  ADD KEY `fk_origen_destino_origen1_idx` (`id_origen`),
  ADD KEY `fk_origen_destino_destino1_idx` (`id_destino`),
  ADD KEY `fk_origen_destino_clase1_idx` (`clase`);

--
-- Indices de la tabla `pasajero`
--
ALTER TABLE `pasajero`
  ADD PRIMARY KEY (`id_pasajero`),
  ADD KEY `tipo_documento_idx` (`tipo_documento`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`id_documento`);

--
-- Indices de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`id_tipo_pago`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indices de la tabla `tipo_vuelo`
--
ALTER TABLE `tipo_vuelo`
  ADD PRIMARY KEY (`id_tipo_vuelo`);

--
-- Indices de la tabla `tiquete`
--
ALTER TABLE `tiquete`
  ADD PRIMARY KEY (`id_tiquete`),
  ADD KEY `id_pasajero_idx` (`id_pasajero`),
  ADD KEY `destino_idx` (`desde_hacia`),
  ADD KEY `fk_tiquete_tipo_pago1_idx` (`tipo_pago`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aerolinea`
--
ALTER TABLE `aerolinea`
  MODIFY `id_aerolinea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `avion`
--
ALTER TABLE `avion`
  MODIFY `id_avion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `avion_destino`
--
ALTER TABLE `avion_destino`
  MODIFY `id_avion_destino` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `clase`
--
ALTER TABLE `clase`
  MODIFY `id_clase` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `destino`
--
ALTER TABLE `destino`
  MODIFY `id_destino` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `login_usuario`
--
ALTER TABLE `login_usuario`
  MODIFY `id_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `origen`
--
ALTER TABLE `origen`
  MODIFY `id_origen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `origen_destino`
--
ALTER TABLE `origen_destino`
  MODIFY `id_origen_destino` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `pasajero`
--
ALTER TABLE `pasajero`
  MODIFY `id_pasajero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `id_documento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `id_tipo_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id_tipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_vuelo`
--
ALTER TABLE `tipo_vuelo`
  MODIFY `id_tipo_vuelo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tiquete`
--
ALTER TABLE `tiquete`
  MODIFY `id_tiquete` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `avion`
--
ALTER TABLE `avion`
  ADD CONSTRAINT `aerolinea` FOREIGN KEY (`aerolinea`) REFERENCES `aerolinea` (`id_aerolinea`);

--
-- Filtros para la tabla `avion_destino`
--
ALTER TABLE `avion_destino`
  ADD CONSTRAINT `fk_avion_destino_origen_destino1` FOREIGN KEY (`id_origen_destino`) REFERENCES `origen_destino` (`id_origen_destino`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_avion_destino_tipo_vuelo1` FOREIGN KEY (`tipo_vuelo`) REFERENCES `tipo_vuelo` (`id_tipo_vuelo`),
  ADD CONSTRAINT `id_avion` FOREIGN KEY (`id_avion`) REFERENCES `avion` (`id_avion`);

--
-- Filtros para la tabla `login_usuario`
--
ALTER TABLE `login_usuario`
  ADD CONSTRAINT `tipo_usuario` FOREIGN KEY (`tipo_usuario`) REFERENCES `tipo_usuario` (`id_tipo`);

--
-- Filtros para la tabla `origen_destino`
--
ALTER TABLE `origen_destino`
  ADD CONSTRAINT `fk_origen_destino_clase1` FOREIGN KEY (`clase`) REFERENCES `clase` (`id_clase`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_origen_destino_destino1` FOREIGN KEY (`id_destino`) REFERENCES `destino` (`id_destino`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_origen_destino_origen1` FOREIGN KEY (`id_origen`) REFERENCES `origen` (`id_origen`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pasajero`
--
ALTER TABLE `pasajero`
  ADD CONSTRAINT `id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `login_usuario` (`id_login`),
  ADD CONSTRAINT `tipo_documento` FOREIGN KEY (`tipo_documento`) REFERENCES `tipo_documento` (`id_documento`);

--
-- Filtros para la tabla `tiquete`
--
ALTER TABLE `tiquete`
  ADD CONSTRAINT `destino` FOREIGN KEY (`desde_hacia`) REFERENCES `avion_destino` (`id_avion_destino`),
  ADD CONSTRAINT `fk_tiquete_tipo_pago1` FOREIGN KEY (`tipo_pago`) REFERENCES `tipo_pago` (`id_tipo_pago`),
  ADD CONSTRAINT `id_pasajero` FOREIGN KEY (`id_pasajero`) REFERENCES `pasajero` (`id_pasajero`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
