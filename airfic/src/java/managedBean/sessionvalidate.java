/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import persistencia.LoginUsuario;

/**
 *
 * @author jonathan.rodriguez
 */
@ManagedBean
@RequestScoped
public class sessionvalidate implements Serializable {

    private String logs;
    private String usuario;
    private LoginUsuario l;

    @PostConstruct
    public void init() {
        this.l = (LoginUsuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("a");
    }

    public void validarSession() {
        try {
            LoginUsuario log = (LoginUsuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("a");

            if (log == null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("faces/index.xhtml");
            }
        } catch (Exception e) {
        }
    }


    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the logs
     */
    public String getLogs() {
        return logs;
    }

    /**
     * @param logs the logs to set
     */
    public void setLogs(String logs) {
        this.logs = logs;
    }

    /**
     * @return the l
     */
    public LoginUsuario getL() {
        return l;
    }

    /**
     * @param l the l to set
     */
    public void setL(LoginUsuario l) {
        this.l = l;
    }

}
