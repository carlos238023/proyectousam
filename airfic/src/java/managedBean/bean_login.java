/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import mantenimiento.mantenimiento_login;
import persistencia.LoginUsuario;
import persistencia.TipoUsuario;

/**
 *
 * @author jonathan.rodriguez
 */
@ManagedBean
@RequestScoped
public class bean_login {

    private LoginUsuario log;
    mantenimiento_login m = new mantenimiento_login();

    @PostConstruct
    public void init() {
        this.setLog(new LoginUsuario());
    }

    public void guardar() throws IOException {
        String mensaje = "";

        TipoUsuario u = new TipoUsuario();
        u.setIdTipo(4);
        log.setTipoUsuario(u);
        LoginUsuario l = m.consultarUss(log.getUsuario());
        if (l == null) {
            m.guardar(log);
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");

        } else {
            mensaje = "Error, usuario ya existe";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @return the log
     */
    public LoginUsuario getLog() {
        return log;
    }

    /**
     * @param log the log to set
     */
    public void setLog(LoginUsuario log) {
        this.log = log;
    }

}
