/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jonathan.rodriguez
 */
@Entity
@Table(name = "tiquete")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tiquete.findAll", query = "SELECT t FROM Tiquete t")
    , @NamedQuery(name = "Tiquete.findByIdTiquete", query = "SELECT t FROM Tiquete t WHERE t.idTiquete = :idTiquete")
    , @NamedQuery(name = "Tiquete.findByValor", query = "SELECT t FROM Tiquete t WHERE t.valor = :valor")
    , @NamedQuery(name = "Tiquete.findByFechaVuelo", query = "SELECT t FROM Tiquete t WHERE t.fechaVuelo = :fechaVuelo")
    , @NamedQuery(name = "Tiquete.findByNumeroPuesto", query = "SELECT t FROM Tiquete t WHERE t.numeroPuesto = :numeroPuesto")
    , @NamedQuery(name = "Tiquete.findByIva", query = "SELECT t FROM Tiquete t WHERE t.iva = :iva")
    , @NamedQuery(name = "Tiquete.findByTotal", query = "SELECT t FROM Tiquete t WHERE t.total = :total")
    , @NamedQuery(name = "Tiquete.findByHoraSalida", query = "SELECT t FROM Tiquete t WHERE t.horaSalida = :horaSalida")
    , @NamedQuery(name = "Tiquete.findByCodigo", query = "SELECT t FROM Tiquete t WHERE t.codigo = :codigo")})
public class Tiquete implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tiquete")
    private Integer idTiquete;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private Double valor;
    @Column(name = "fecha_vuelo")
    private String fechaVuelo;
    @Column(name = "numero_puesto")
    private Integer numeroPuesto;
    @Column(name = "iva")
    private Double iva;
    @Column(name = "total")
    private Double total;
    @Column(name = "hora_salida")
    private String horaSalida;
    @Column(name = "codigo")
    private Integer codigo;
    @JoinColumn(name = "desde_hacia", referencedColumnName = "id_avion_destino")
    @ManyToOne
    private AvionDestino desdeHacia;
    @JoinColumn(name = "tipo_pago", referencedColumnName = "id_tipo_pago")
    @ManyToOne
    private TipoPago tipoPago;
    @JoinColumn(name = "id_pasajero", referencedColumnName = "id_pasajero")
    @ManyToOne
    private Pasajero idPasajero;

    public Tiquete() {
    }

    public Tiquete(Integer idTiquete) {
        this.idTiquete = idTiquete;
    }

    public Integer getIdTiquete() {
        return idTiquete;
    }

    public void setIdTiquete(Integer idTiquete) {
        this.idTiquete = idTiquete;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getFechaVuelo() {
        return fechaVuelo;
    }

    public void setFechaVuelo(String fechaVuelo) {
        this.fechaVuelo = fechaVuelo;
    }

    public Integer getNumeroPuesto() {
        return numeroPuesto;
    }

    public void setNumeroPuesto(Integer numeroPuesto) {
        this.numeroPuesto = numeroPuesto;
    }

    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public AvionDestino getDesdeHacia() {
        return desdeHacia;
    }

    public void setDesdeHacia(AvionDestino desdeHacia) {
        this.desdeHacia = desdeHacia;
    }

    public TipoPago getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(TipoPago tipoPago) {
        this.tipoPago = tipoPago;
    }

    public Pasajero getIdPasajero() {
        return idPasajero;
    }

    public void setIdPasajero(Pasajero idPasajero) {
        this.idPasajero = idPasajero;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTiquete != null ? idTiquete.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tiquete)) {
            return false;
        }
        Tiquete other = (Tiquete) object;
        if ((this.idTiquete == null && other.idTiquete != null) || (this.idTiquete != null && !this.idTiquete.equals(other.idTiquete))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.Tiquete[ idTiquete=" + idTiquete + " ]";
    }
    
}
