/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimiento;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import persistencia.LoginUsuario;

/**
 *
 * @author roberto.hernandezUSA
 */
public class mantenimiento_login {

    public boolean guardar(LoginUsuario objeto) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        try {
            em.persist(objeto);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            em.getTransaction().rollback();
            return false;
        } finally {
            em.close();
        }
    }

    public boolean borrar(int id) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        LoginUsuario objeto = null;
        try {
            objeto = em.find(LoginUsuario.class, id);
            em.remove(objeto);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            em.getTransaction().rollback();
            return false;
        } finally {
            em.close();
        }
    }

    public LoginUsuario consultar(int id) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        LoginUsuario objeto = null;
        try {
            objeto = em.find(LoginUsuario.class, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

    public boolean actualizar(LoginUsuario objeto) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        LoginUsuario objeto2 = null;
        try {
            objeto2 = em.find(LoginUsuario.class, objeto.getIdLogin());
            objeto2.setUsuario(objeto.getUsuario());
            objeto2.setPass(objeto.getPass());
            objeto2.setTipoUsuario(objeto.getTipoUsuario());
            objeto2.setPregunta(objeto.getPregunta());
            objeto2.setPregunta(objeto.getRespuesta());
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            em.getTransaction().rollback();
            return false;
        } finally {
            em.close();
        }

    }

    public List consultarTodo() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<LoginUsuario> lista = null;

        try {
            Query q = em.createQuery("SELECT l FROM Login l");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public LoginUsuario consultarUss(String us) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();

        Query q = em.createNamedQuery("LoginUsuario.findByUsuario")
                .setParameter("usuario", us);

        LoginUsuario log = null;

        List<LoginUsuario> lista = q.getResultList();

        for (LoginUsuario login : lista) {
            log = login;
        }
        em.close();
        return log;
    }

}
